package xxhash

import (
	"bytes"
	"encoding/binary"
)

func readLe32(b *bytes.Buffer) (lane uint32) {
    lane = binary.LittleEndian.Uint32(b.Next(4))
	return
}

func readLe64(b *bytes.Buffer) (lane uint64) {
    lane = binary.LittleEndian.Uint64(b.Next(8))
	return
}
