[![pipeline status](https://gitlab.com/hesperos/xxhash/badges/master/pipeline.svg)](https://gitlab.com/hesperos/xxhash/commits/master)
[![coverage report](https://gitlab.com/hesperos/xxhash/badges/master/coverage.svg)](https://gitlab.com/hesperos/xxhash/commits/master)

# golang xxhash

This package implements [xxhash](http://cyan4973.github.io/xxHash/)
hashing functions in context of Hash32/Hash64 golang interfaces.

## Usage

This package implements standard hash.Hash32 and hash.Hash64 interface.
Use it just like any other implementation of these interfaces (like crc32
or crc64). Below are some examples.

### Calculating the hash of the data read from a file:

```
seed := 0
h := xxhash.NewXxh32(seed)

f, _ := os.Open("a/path/to/some/file")
r := io.TeeReader(f, h)

data := ioutil.ReadAll(r)
fmt.Println("the hash of the file contents:", h.Sum32())
```

### Calculating the hash of []byte:

```
h := xxhash.NewXxh32(seed)
data := []byte{ "some data" }
fmt.Println("hash of the slice contents:", h.Sum32())
```
