package xxhash

import (
	"testing"
)

func TestShl32Wrapping(t *testing.T) {
	testData := []struct {
		given, n, expected uint32
	}{
		{0x0000ffff, 0, 0x0000ffff},
		{0x12345678, 0, 0x12345678},
		{0xffffffff, 0, 0xffffffff},
		{0xffff0000, 0, 0xffff0000},
		{0x0000ffff, 4, 0x000ffff0},
		{0x12345678, 4, 0x23456781},
		{0xffffffff, 4, 0xffffffff},
		{0xffff0000, 4, 0xfff0000f},
		{0x0000ffff, 8, 0x00ffff00},
		{0x12345678, 8, 0x34567812},
		{0xffffffff, 8, 0xffffffff},
		{0xffff0000, 8, 0xff0000ff},
		{0x0000ffff, 16, 0xffff0000},
		{0x12345678, 16, 0x56781234},
		{0xffffffff, 16, 0xffffffff},
		{0xffff0000, 16, 0x0000ffff},
		{0x0000ffff, 24, 0xff0000ff},
		{0x12345678, 24, 0x78123456},
		{0xffffffff, 24, 0xffffffff},
		{0xffff0000, 24, 0x00ffff00},
		{0x0000ffff, 32, 0x0000ffff},
		{0x12345678, 32, 0x12345678},
		{0xffffffff, 32, 0xffffffff},
		{0xffff0000, 32, 0xffff0000},
	}

	for _, data := range testData {
		result := shl32(data.given, data.n)
		if result != data.expected {
			t.Errorf("%x <<< %d != %x(%x)\n",
				data.given, data.n, data.expected, result)
		}
	}
}

func TestShr32Wrapping(t *testing.T) {
	testData := []struct {
		given, n, expected uint32
	}{
		{0x0000ffff, 0, 0x0000ffff},
		{0x12345678, 0, 0x12345678},
		{0xffffffff, 0, 0xffffffff},
		{0xffff0000, 0, 0xffff0000},
		{0x0000ffff, 4, 0xf0000fff},
		{0x12345678, 4, 0x81234567},
		{0xffffffff, 4, 0xffffffff},
		{0xffff0000, 4, 0x0ffff000},
		{0x0000ffff, 8, 0xff0000ff},
		{0x12345678, 8, 0x78123456},
		{0xffffffff, 8, 0xffffffff},
		{0xffff0000, 8, 0x00ffff00},
		{0x0000ffff, 16, 0xffff0000},
		{0x12345678, 16, 0x56781234},
		{0xffffffff, 16, 0xffffffff},
		{0xffff0000, 16, 0x0000ffff},
		{0x0000ffff, 24, 0x00ffff00},
		{0x12345678, 24, 0x34567812},
		{0xffffffff, 24, 0xffffffff},
		{0xffff0000, 24, 0xff0000ff},
		{0x0000ffff, 32, 0x0000ffff},
		{0x12345678, 32, 0x12345678},
		{0xffffffff, 32, 0xffffffff},
		{0xffff0000, 32, 0xffff0000},
	}

	for _, data := range testData {
		result := shr32(data.given, data.n)
		if result != data.expected {
			t.Errorf("%x <<< %d != %x(%x)\n",
				data.given, data.n, data.expected, result)
		}
	}
}

func TestShl64Wrapping(t *testing.T) {
	testData := []struct {
		given, n, expected uint64
	}{
		{0x00000000ffffffff, 0, 0x00000000ffffffff},
		{0x0123456789abcdef, 0, 0x0123456789abcdef},
		{0xffffffffffffffff, 0, 0xffffffffffffffff},
		{0xffffffff00000000, 0, 0xffffffff00000000},
		{0x00000000ffffffff, 4, 0x0000000ffffffff0},
		{0x0123456789abcdef, 4, 0x123456789abcdef0},
		{0xffffffffffffffff, 4, 0xffffffffffffffff},
		{0xffffffff00000000, 4, 0xfffffff00000000f},
		{0x00000000ffffffff, 8, 0x000000ffffffff00},
		{0x0123456789abcdef, 8, 0x23456789abcdef01},
		{0xffffffffffffffff, 8, 0xffffffffffffffff},
		{0xffffffff00000000, 8, 0xffffff00000000ff},
		{0x00000000ffffffff, 16, 0x0000ffffffff0000},
		{0x0123456789abcdef, 16, 0x456789abcdef0123},
		{0xffffffffffffffff, 16, 0xffffffffffffffff},
		{0xffffffff00000000, 16, 0xffff00000000ffff},
		{0x00000000ffffffff, 24, 0x00ffffffff000000},
		{0x0123456789abcdef, 24, 0x6789abcdef012345},
		{0xffffffffffffffff, 24, 0xffffffffffffffff},
		{0xffffffff00000000, 24, 0xff00000000ffffff},
		{0x00000000ffffffff, 32, 0xffffffff00000000},
		{0x0123456789abcdef, 32, 0x89abcdef01234567},
		{0xffffffffffffffff, 32, 0xffffffffffffffff},
		{0xffffffff00000000, 32, 0x00000000ffffffff},
		{0x00000000ffffffff, 40, 0xffffff00000000ff},
		{0x0123456789abcdef, 40, 0xabcdef0123456789},
		{0xffffffffffffffff, 40, 0xffffffffffffffff},
		{0xffffffff00000000, 40, 0x000000ffffffff00},
		{0x00000000ffffffff, 48, 0xffff00000000ffff},
		{0x0123456789abcdef, 48, 0xcdef0123456789ab},
		{0xffffffffffffffff, 48, 0xffffffffffffffff},
		{0xffffffff00000000, 48, 0x0000ffffffff0000},
		{0x00000000ffffffff, 56, 0xff00000000ffffff},
		{0x0123456789abcdef, 56, 0xef0123456789abcd},
		{0xffffffffffffffff, 56, 0xffffffffffffffff},
		{0xffffffff00000000, 56, 0x00ffffffff000000},
		{0x00000000ffffffff, 64, 0x00000000ffffffff},
		{0x0123456789abcdef, 64, 0x0123456789abcdef},
		{0xffffffffffffffff, 64, 0xffffffffffffffff},
		{0xffffffff00000000, 64, 0xffffffff00000000},
	}

	for _, data := range testData {
		result := shl64(data.given, data.n)
		if result != data.expected {
			t.Errorf("%x <<< %d != %x(%x)\n",
				data.given, data.n, data.expected, result)
		}
	}
}

func TestShr64Wrapping(t *testing.T) {
	testData := []struct {
		given, n, expected uint64
	}{
		{0x00000000ffffffff, 0, 0x00000000ffffffff},
		{0x0123456789abcdef, 0, 0x0123456789abcdef},
		{0xffffffffffffffff, 0, 0xffffffffffffffff},
		{0xffffffff00000000, 0, 0xffffffff00000000},
		{0x00000000ffffffff, 4, 0xf00000000fffffff},
		{0x0123456789abcdef, 4, 0xf0123456789abcde},
		{0xffffffffffffffff, 4, 0xffffffffffffffff},
		{0xffffffff00000000, 4, 0x0ffffffff0000000},
		{0x00000000ffffffff, 8, 0xff00000000ffffff},
		{0x0123456789abcdef, 8, 0xef0123456789abcd},
		{0xffffffffffffffff, 8, 0xffffffffffffffff},
		{0xffffffff00000000, 8, 0x00ffffffff000000},
		{0x00000000ffffffff, 16, 0xffff00000000ffff},
		{0x0123456789abcdef, 16, 0xcdef0123456789ab},
		{0xffffffffffffffff, 16, 0xffffffffffffffff},
		{0xffffffff00000000, 16, 0x0000ffffffff0000},
		{0x00000000ffffffff, 24, 0xffffff00000000ff},
		{0x0123456789abcdef, 24, 0xabcdef0123456789},
		{0xffffffffffffffff, 24, 0xffffffffffffffff},
		{0xffffffff00000000, 24, 0x000000ffffffff00},
		{0x00000000ffffffff, 32, 0xffffffff00000000},
		{0x0123456789abcdef, 32, 0x89abcdef01234567},
		{0xffffffffffffffff, 32, 0xffffffffffffffff},
		{0xffffffff00000000, 32, 0x00000000ffffffff},
		{0x00000000ffffffff, 40, 0x00ffffffff000000},
		{0x0123456789abcdef, 40, 0x6789abcdef012345},
		{0xffffffffffffffff, 40, 0xffffffffffffffff},
		{0xffffffff00000000, 40, 0xff00000000ffffff},
		{0x00000000ffffffff, 48, 0x0000ffffffff0000},
		{0x0123456789abcdef, 48, 0x456789abcdef0123},
		{0xffffffffffffffff, 48, 0xffffffffffffffff},
		{0xffffffff00000000, 48, 0xffff00000000ffff},
		{0x00000000ffffffff, 56, 0x000000ffffffff00},
		{0x0123456789abcdef, 56, 0x23456789abcdef01},
		{0xffffffffffffffff, 56, 0xffffffffffffffff},
		{0xffffffff00000000, 56, 0xffffff00000000ff},
		{0x00000000ffffffff, 64, 0x00000000ffffffff},
		{0x0123456789abcdef, 64, 0x0123456789abcdef},
		{0xffffffffffffffff, 64, 0xffffffffffffffff},
		{0xffffffff00000000, 64, 0xffffffff00000000},
	}

	for _, data := range testData {
		result := shr64(data.given, data.n)
		if result != data.expected {
			t.Errorf("%x <<< %d != %x(%x)\n",
				data.given, data.n, data.expected, result)
		}
	}

}
