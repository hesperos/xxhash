package xxhash

import (
	"testing"
)

type testData32 struct {
	given    string
	seed     uint32
	expected uint32
}

var td32 []testData32 = []testData32{
	{"lorem ipsum", 0, 0xd8050048},
	{"mary had a little lamb", 0, 0x8adcea48},
	{"The quick brown fox jumps over the lazy dog", 0, 0xe85ea4de},
	{"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus enim, molestie id eros vitae, luctus vestibulum nisi. Suspendisse consectetur semper porta. Nullam at metus vehicula, ornare purus iaculis, tempus leo. Vivamus non nisl eget urna consequat tempus ac ut nulla. Nulla facilisi. Ut dictum at nibh at consequat. Maecenas sit amet porta tortor. Nunc laoreet metus sed ornare sollicitudin. Pellentesque odio tellus, scelerisque sed sapien at, faucibus facilisis ex. Duis sem lorem, luctus et dolor consequat, tincidunt porttitor nulla. Pellentesque quis arcu efficitur enim viverra venenatis. Pellentesque varius commodo ultrices. Integer quis arcu in sapien vestibulum pharetra.", 0, 0x7cae045d},
	{"lorem ipsum", 1234, 0x219d38bd},
	{"mary had a little lamb", 1234, 0x29d60e4b},
	{"The quick brown fox jumps over the lazy dog", 1234, 0xf7580370},
	{"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus enim, molestie id eros vitae, luctus vestibulum nisi. Suspendisse consectetur semper porta. Nullam at metus vehicula, ornare purus iaculis, tempus leo. Vivamus non nisl eget urna consequat tempus ac ut nulla. Nulla facilisi. Ut dictum at nibh at consequat. Maecenas sit amet porta tortor. Nunc laoreet metus sed ornare sollicitudin. Pellentesque odio tellus, scelerisque sed sapien at, faucibus facilisis ex. Duis sem lorem, luctus et dolor consequat, tincidunt porttitor nulla. Pellentesque quis arcu efficitur enim viverra venenatis. Pellentesque varius commodo ultrices. Integer quis arcu in sapien vestibulum pharetra.", 1234, 0x48d6c557},
}

func TestXxh32WithShortStringInput(t *testing.T) {
	for _, data := range td32 {
		xxh := NewXxh32(data.seed)
		xxh.Write([]byte(data.given))
		h := xxh.Sum32()
		if h != data.expected {
			t.Errorf("hash %x != %x for [%s](%x)\n",
				h, data.expected, data.given, data.seed)
		}
	}
}

func TestXxh32Reset(t *testing.T) {
	const nAttempts = 32
	for _, data := range td32 {
		xxh := NewXxh32(data.seed)
		for i := 0; i < nAttempts; i++ {
			xxh.Write([]byte(data.given))
			h := xxh.Sum32()
			if h != data.expected {
				t.Errorf("hash %x != %x for [%s](%x)\n",
					h, data.expected, data.given, data.seed)
			}
			xxh.Reset()
		}
	}
}

func BenchmarkXxh32WithPreparedInput(b *testing.B) {
	content := []byte(td32[len(td32)-1].given)
	xxh := NewXxh32(123)
	for n := 0; n < b.N; n++ {
		xxh.Reset()
		xxh.Write(content)
		xxh.Sum32()
	}
}

func BenchmarkXxh32WithPreparedInputNoReset(b *testing.B) {
	content := []byte(td32[len(td32)-1].given)
	xxh := NewXxh32(123)
	for n := 0; n < b.N; n++ {
		xxh.Write(content)
		xxh.Sum32()
	}
}
