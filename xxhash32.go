package xxhash

import (
	"bytes"
	"fmt"
	"hash"
)

const (
	prime32_1 uint32 = 2654435761
	prime32_2 uint32 = 2246822519
	prime32_3 uint32 = 3266489917
	prime32_4 uint32 = 668265263
	prime32_5 uint32 = 374761393
)

const (
	stripeSize32 = 16
	lanes32      = 4
	laneSize32   = 4
)

type xxh32 struct {
	// accumulator
	accu uint32

	// stripe accumulators
	stripeAccu [4]uint32

	// seed
	seed uint32

	// temporary data store
	stripeBuffer bytes.Buffer

	// total length of consumed input
	length int
}

func (h *xxh32) converge() {
	h.accu = shl32(h.stripeAccu[0], 1) +
		shl32(h.stripeAccu[1], 7) +
		shl32(h.stripeAccu[2], 12) +
		shl32(h.stripeAccu[3], 18)
}

func (h *xxh32) avalanche() {
	h.accu = h.accu ^ (h.accu >> 15)
	h.accu = h.accu * prime32_2
	h.accu = h.accu ^ (h.accu >> 13)
	h.accu = h.accu * prime32_3
	h.accu = h.accu ^ (h.accu >> 16)
}

func (h *xxh32) round(lane uint32, nAccu int) {
	h.stripeAccu[nAccu] += (lane * prime32_2)
	h.stripeAccu[nAccu] = shl32(h.stripeAccu[nAccu], 13)
	h.stripeAccu[nAccu] *= prime32_1
}

func (h *xxh32) Write(p []byte) (int, error) {

	// append new data
	h.stripeBuffer.Write(p)
	dataLen := len(p)
	h.length += dataLen

	// invalidate cached value
	h.accu = 0

	// read and round the full blocks
	for h.stripeBuffer.Len() >= stripeSize32 {
		for nAccu := 0; nAccu < lanes32; nAccu++ {
			lane := readLe32(&h.stripeBuffer)
			h.round(lane, nAccu)
		}
	}

	return dataLen, nil
}

func (h *xxh32) Sum(b []byte) []byte {
	digest := h.Sum32()
	return append(b,
		byte(digest>>24),
		byte(digest>>16),
		byte(digest>>8),
		byte(digest>>0))
}

func (h *xxh32) Reset() {
	// initialise to use with output < 16
	h.stripeAccu[0] = h.seed + prime32_1 + prime32_2
	h.stripeAccu[1] = h.seed + prime32_2
	h.stripeAccu[2] = h.seed + 0
	h.stripeAccu[3] = h.seed - prime32_1

	h.accu = 0
	h.length = 0
	h.stripeBuffer.Reset()
}

func (h *xxh32) Size() int {
	return lanes32
}

func (h *xxh32) BlockSize() int {
	return stripeSize32
}

func (h *xxh32) finalise() error {
	for h.stripeBuffer.Len() >= laneSize32 {
		lane := readLe32(&h.stripeBuffer)
		h.accu += lane * prime32_3
		h.accu = shl32(h.accu, 17) * prime32_4
	}

	for h.stripeBuffer.Len() > 0 {
		var lane uint32
		tmp, err := h.stripeBuffer.ReadByte()
		if err != nil {
			return fmt.Errorf("unable to finalise")
		}

		lane = (uint32)(tmp)
		h.accu += lane * prime32_5
		h.accu = shl32(h.accu, 11) * prime32_1
	}

	return nil
}

func (h *xxh32) Sum32() (digest uint32) {
	if h.accu == 0 {
		if h.length < stripeSize32 {
			h.accu = h.stripeAccu[2] + prime32_5
		} else {
			h.converge()
		}

		h.accu += (uint32)(h.length)
		h.finalise()
		h.avalanche()
	}

	digest = h.accu
	return
}

// This function returns new hash.Hash32 which computes xxhash32 hash.
// Its Sum32 function will return the hash digest in big-endian byte order.
func NewXxh32(seed uint32) hash.Hash32 {
	xxh := xxh32{
		seed: seed,
	}
	xxh.stripeBuffer.Grow(stripeSize32)
	xxh.Reset()
	return &xxh
}
