package xxhash

func shl32(i, n uint32) uint32 {
	lower := i >> (32 - n)
	return (i << n) | lower
}

func shr32(i, n uint32) uint32 {
	higher := i << (32 - n)
	return (i >> n) | higher
}

func shl64(i, n uint64) uint64 {
	lower := i >> (64 - n)
	return (i << n) | lower
}

func shr64(i, n uint64) uint64 {
	higher := i << (64 - n)
	return (i >> n) | higher
}
