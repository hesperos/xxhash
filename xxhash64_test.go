package xxhash

import (
	"testing"
)

type testData64 struct {
	given    string
	seed     uint64
	expected uint64
}

var td64 []testData64 = []testData64{
	{"lorem ipsum", 0, 0xf2a3da3883851a6e},
	{"mary had a little lamb", 0, 0x0ae978e227090a86},
	{"The quick brown fox jumps over the lazy dog", 0, 0x0b242d361fda71bc},
	{"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus enim, molestie id eros vitae, luctus vestibulum nisi. Suspendisse consectetur semper porta. Nullam at metus vehicula, ornare purus iaculis, tempus leo. Vivamus non nisl eget urna consequat tempus ac ut nulla. Nulla facilisi. Ut dictum at nibh at consequat. Maecenas sit amet porta tortor. Nunc laoreet metus sed ornare sollicitudin. Pellentesque odio tellus, scelerisque sed sapien at, faucibus facilisis ex. Duis sem lorem, luctus et dolor consequat, tincidunt porttitor nulla. Pellentesque quis arcu efficitur enim viverra venenatis. Pellentesque varius commodo ultrices. Integer quis arcu in sapien vestibulum pharetra.", 0, 0x5eb430de25eb4d4c},
	{"lorem ipsum", 1234, 0x627230f498926cf8},
	{"mary had a little lamb", 1234, 0x378a9c272fcca1ce},
	{"The quick brown fox jumps over the lazy dog", 1234, 0x5b3c21fecfc907fd},
	{"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus enim, molestie id eros vitae, luctus vestibulum nisi. Suspendisse consectetur semper porta. Nullam at metus vehicula, ornare purus iaculis, tempus leo. Vivamus non nisl eget urna consequat tempus ac ut nulla. Nulla facilisi. Ut dictum at nibh at consequat. Maecenas sit amet porta tortor. Nunc laoreet metus sed ornare sollicitudin. Pellentesque odio tellus, scelerisque sed sapien at, faucibus facilisis ex. Duis sem lorem, luctus et dolor consequat, tincidunt porttitor nulla. Pellentesque quis arcu efficitur enim viverra venenatis. Pellentesque varius commodo ultrices. Integer quis arcu in sapien vestibulum pharetra.", 1234, 0x47779bc7569ee025},
}

func TestXxh64WithShortStringInput(t *testing.T) {
	for _, data := range td64 {
		xxh := NewXxh64(data.seed)
		xxh.Write([]byte(data.given))
		h := xxh.Sum64()
		if h != data.expected {
			t.Errorf("hash %x != %x for [%s](%x)\n",
				h, data.expected, data.given, data.seed)
		}
	}
}

func TestXxh64Reset(t *testing.T) {
	const nAttempts = 32
	for _, data := range td64 {
		xxh := NewXxh64(data.seed)
		for i := 0; i < nAttempts; i++ {
			xxh.Write([]byte(data.given))
			h := xxh.Sum64()
			if h != data.expected {
				t.Errorf("hash %x != %x for [%s](%x)\n",
					h, data.expected, data.given, data.seed)
			}
			xxh.Reset()
		}
	}
}

func BenchmarkXxh64WithPreparedInput(b *testing.B) {
	content := []byte(td64[len(td64)-1].given)
	xxh := NewXxh64(123)
	for n := 0; n < b.N; n++ {
		xxh.Reset()
		xxh.Write(content)
		xxh.Sum64()
	}
}

func BenchmarkXxh64WithPreparedInputNoReset(b *testing.B) {
	content := []byte(td64[len(td64)-1].given)
	xxh := NewXxh64(123)
	for n := 0; n < b.N; n++ {
		xxh.Write(content)
		xxh.Sum64()
	}
}
