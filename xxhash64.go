// Implementation of xxhash32/xxhash64 flavours of xxhash digest.
// This hashing function is described at:
// http://cyan4973.github.io/xxHash
//
// Use NewXxh32/Newxxh64 constructor functions in order
// to get the hash.Hash32/hash.Hash64 object.
package xxhash

import (
	"bytes"
	"fmt"
	"hash"
)

const (
	prime64_1 uint64 = 11400714785074694791
	prime64_2 uint64 = 14029467366897019727
	prime64_3 uint64 = 1609587929392839161
	prime64_4 uint64 = 9650029242287828579
	prime64_5 uint64 = 2870177450012600261
)

const (
	stripeSize64 = 32
	lanes64      = 4
	laneSize64   = 8
)

type xxh64 struct {
	// accumulator
	accu uint64

	// stripe accumulators
	stripeAccu [4]uint64

	// seed
	seed uint64

	// temporary data store
	stripeBuffer bytes.Buffer

	// total length of consumed input
	length int
}

func mergeAccu(accu, stripeAccu uint64) uint64 {
	accu = accu ^ round(0, stripeAccu)
	accu = accu * prime64_1
	return accu + prime64_4
}

func (h *xxh64) converge() {
	h.accu = shl64(h.stripeAccu[0], 1) +
		shl64(h.stripeAccu[1], 7) +
		shl64(h.stripeAccu[2], 12) +
		shl64(h.stripeAccu[3], 18)

	h.accu = mergeAccu(h.accu, h.stripeAccu[0])
	h.accu = mergeAccu(h.accu, h.stripeAccu[1])
	h.accu = mergeAccu(h.accu, h.stripeAccu[2])
	h.accu = mergeAccu(h.accu, h.stripeAccu[3])
}

func (h *xxh64) avalanche() {
	h.accu = h.accu ^ (h.accu >> 33)
	h.accu = h.accu * prime64_2
	h.accu = h.accu ^ (h.accu >> 29)
	h.accu = h.accu * prime64_3
	h.accu = h.accu ^ (h.accu >> 32)
}

func round(accu, lane uint64) uint64 {
	accu += lane * prime64_2
	accu = shl64(accu, 31)
	accu *= prime64_1
	return accu
}

func (h *xxh64) Write(p []byte) (int, error) {
	// append new data
	h.stripeBuffer.Write(p)
	dataLen := len(p)
	h.length += dataLen

	// invalidate cached value
	h.accu = 0

	// read and round the full blocks
	for h.stripeBuffer.Len() >= stripeSize64 {
		for nAccu := 0; nAccu < lanes64; nAccu++ {
			lane := readLe64(&h.stripeBuffer)
			h.stripeAccu[nAccu] = round(h.stripeAccu[nAccu], lane)
		}
	}

	return dataLen, nil
}

func (h *xxh64) Sum(b []byte) []byte {
	digest := h.Sum64()
	return append(b,
		byte(digest>>56),
		byte(digest>>48),
		byte(digest>>40),
		byte(digest>>32),
		byte(digest>>24),
		byte(digest>>16),
		byte(digest>>8),
		byte(digest>>0))
}

func (h *xxh64) Reset() {
	h.stripeAccu[0] = h.seed + prime64_1 + prime64_2
	h.stripeAccu[1] = h.seed + prime64_2
	h.stripeAccu[2] = h.seed + 0
	h.stripeAccu[3] = h.seed - prime64_1

	h.accu = 0
	h.length = 0
	h.stripeBuffer.Reset()
}

func (h *xxh64) Size() int {
	return lanes64
}

func (h *xxh64) BlockSize() int {
	return stripeSize64
}

func (h *xxh64) finalise() error {
	for h.stripeBuffer.Len() >= laneSize64 {
		lane := readLe64(&h.stripeBuffer)
		h.accu = h.accu ^ round(0, lane)
		h.accu = shl64(h.accu, 27) * prime64_1
		h.accu += prime64_4
	}

	for h.stripeBuffer.Len() >= laneSize32 {
		lane := readLe32(&h.stripeBuffer)
		h.accu = h.accu ^ ((uint64)(lane) * prime64_1)
		h.accu = shl64(h.accu, 23) * prime64_2
		h.accu += prime64_3
	}

	for h.stripeBuffer.Len() > 0 {
		var lane uint64
		tmp, err := h.stripeBuffer.ReadByte()
		if err != nil {
			return fmt.Errorf("unable to finalise")
		}

		lane = (uint64)(tmp)
		h.accu = h.accu ^ (lane * prime64_5)
		h.accu = shl64(h.accu, 11) * prime64_1
	}

	return nil
}

func (h *xxh64) Sum64() (digest uint64) {
	if h.accu == 0 {

		if h.length < stripeSize64 {
			h.accu = h.stripeAccu[2] + prime64_5
		} else {
			h.converge()
		}

		h.accu += (uint64)(h.length)
		if err := h.finalise(); err != nil {
			h.accu = 0
		}

		h.avalanche()
	}

	digest = h.accu
	return
}

// This function returns new hash.Hash64 which computes xxhash64 hash.
// Its Sum64 function will return the hash digest in big-endian byte order.
func NewXxh64(seed uint64) hash.Hash64 {
	xxh := xxh64{
		seed: seed,
	}
	xxh.stripeBuffer.Grow(stripeSize64)
	xxh.Reset()
	return &xxh
}
